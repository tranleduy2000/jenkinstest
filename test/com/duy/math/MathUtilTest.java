/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.math;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author duy
 */
public class MathUtilTest {
    
    
    @Test
    public void testSuccessCases() {
        
        assertEquals(1, MathUtil.factorial(0));
        
        assertEquals(1, MathUtil.factorial(1));
        
        assertEquals(2, MathUtil.factorial(2));
        
        
        assertEquals(6, MathUtil.factorial(4));
    }
    
   
    @Test(expected = IllegalArgumentException.class)
    public void testFailedCases() {
         MathUtil.factorial(-1);
    }
}
