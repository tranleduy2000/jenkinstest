/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.math;

/**
 *
 * @author duy
 */
public class MathUtil {
    
    public static long factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
    
}
